import 'package:flutter/material.dart';
import 'package:news_app/api/api_get_data.dart';
import 'package:news_app/models/models_api/api_artilces.dart';

class SportScreen extends StatefulWidget {
  const SportScreen({Key? key}) : super(key: key);

  @override
  _SportScreenState createState() => _SportScreenState();
}

class _SportScreenState extends State<SportScreen> {
  late Future<List<Articles>> _futureData;
  late List<Articles> _sport2;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _futureData = ApiRequest().getDataSport();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _refrechData,
      child: FutureBuilder<List<Articles>>(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.redAccent,
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
                child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Icon(
                  Icons.warning,
                  color: Colors.yellow[400],
                  size: 90,
                ),
                const Text(
                  'check your internet',
                  style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w300,
                    fontSize: 30,
                  ),
                )
              ],
            ));
          } else {
            _sport2 = snapshot.data!;
            return ListView.builder(
              itemBuilder: (context, index) {
                Articles items = _sport2.elementAt(index);
                return Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ListTile(
                    leading: items.urlToImage != null
                        ? Image.network(
                            "${items.urlToImage}",
                            height: 100,
                            fit: BoxFit.cover,
                          )
                        : Image.network(
                            'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJSqysINIImhZQaCTMBfF3SpEL9PjmuLw6fA&usqp=CAU',
                            fit: BoxFit.cover,
                          ),
                    title: Text(
                      "${items.title}",
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                    subtitle: Text(
                      "${items.description}",
                      style: TextStyle(color: Color(0xFF757575)),
                    ),
                  ),
                );
              },
              itemCount: _sport2.length,
            );
          }
        },
        future: _futureData,
      ),
    );
  }

  Future _refrechData() async {
    return await ApiRequest().getDataSport();
  }
}

//       FutureBuilder<List<Articles>>(
//         future: sport,
//         builder: (context, snapschot) {
//           if (snapschot.connectionState == ConnectionState.waiting) {
//             return Center(
//               child: CircularProgressIndicator(
//                 color: Colors.orange,
//               ),
//             );
//           } else if (snapschot.hasError) {
//             return Text("Error");
//           } else {
//             _sport2=snapschot.data!;
//             return ListView.builder(
//               itemBuilder: (context, index) {
//                 Articles sportUser = _sport2.elementAt(index);
//                 return ListTile(
//                   // leading: Image.network(
//                   //   "${sportUser.urlToImage}",
//                   //   height: 150,
//                   //   width: 150,
//                   // ),
//                   title: Text("${sportUser.title}"),
//                   trailing: Text("${sportUser.description}"),
//                 );
//
//               },
//               itemCount: _sport2.length,
//             );
//           }
//         });
//   }
// }
