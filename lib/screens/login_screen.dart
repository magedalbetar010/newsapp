import 'package:flutter/material.dart';
import 'package:news_app/api/api_get_data.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  late TextEditingController _mobileControoler;
  late TextEditingController _passwordControoler;

  @override
  void initState() {
    _mobileControoler = TextEditingController();
    _passwordControoler = TextEditingController();

    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.red,
        title: const Text(
          "Login",
          style: TextStyle(color: Colors.white),
        ),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back_ios),
          onPressed: () {},
        ),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              TextFormField(
                keyboardType: TextInputType.phone,
                controller: _mobileControoler,
                decoration: const InputDecoration(
                  label: Text("mobile"),
                  icon: Icon(Icons.mobile_friendly),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              TextFormField(
                obscureText: true,
                keyboardType: TextInputType.text,
                controller: _passwordControoler,
                decoration: const InputDecoration(
                  alignLabelWithHint: true,
                  label: Text("password"),
                  icon: Icon(Icons.https),
                ),
              ),
              const SizedBox(
                height: 15,
              ),
              Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.red, borderRadius: BorderRadius.circular(20)),
                child: TextButton(
                  onPressed: () async {
                    login();
                  },
                  child: const Text(
                    "SingIN",
                    style: TextStyle(color: Colors.white),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
 Future<void> logged()async{
    if(_mobileControoler.text.isNotEmpty&&_passwordControoler.text.isNotEmpty){
      await login();
      
    }
  }

  Future login() async {
    return await ApiRequest().getDataUser(
        id: _mobileControoler.text, password: _passwordControoler.text);
  }
}
