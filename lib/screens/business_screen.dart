import 'package:flutter/material.dart';
import 'package:news_app/api/api_get_data.dart';
import 'package:news_app/models/models_api/api_artilces.dart';

class BusinessScreen extends StatefulWidget {
  const BusinessScreen({Key? key}) : super(key: key);

  @override
  _BusinessScreenState createState() => _BusinessScreenState();
}

class _BusinessScreenState extends State<BusinessScreen> {
  late Future<List<Articles>> _futureDataBas;
  late List<Articles> _bussines;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _futureDataBas = ApiRequest().getDataBusiness();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: _refrechData,
      child: FutureBuilder<List<Articles>>(
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.waiting) {
            return const Center(
              child: CircularProgressIndicator(
                color: Colors.redAccent,
              ),
            );
          } else if (snapshot.hasError) {
            return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.warning,
                      color: Colors.yellow[400],
                      size: 90,
                    ),
                    const Text('check your internet',
                      style: TextStyle(color: Colors.black,
                        fontWeight: FontWeight.w300,
                        fontSize: 30,

                      ),)


                  ],
                ));
          } else {
            _bussines = snapshot.data!;
            return ListView.builder(
              itemBuilder: (context, index) {
                Articles items = _bussines.elementAt(index);
                return Padding(
                  padding: const EdgeInsets.all(12.0),
                  child: ListTile(
                    leading: items.urlToImage != null
                        ? Image.network(
                      "${items.urlToImage}",
                      height: 100,
                      fit: BoxFit.cover,
                    )
                        : Image.network(
                      'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQJSqysINIImhZQaCTMBfF3SpEL9PjmuLw6fA&usqp=CAU',
                      fit: BoxFit.cover,
                    ),
                    title: Text(
                      "${items.title}",
                      style: const TextStyle(
                          fontWeight: FontWeight.bold, color: Colors.black),
                    ),
                    subtitle: Text(
                      "${items.description}",
                      style: const TextStyle(color: Color(0xFF757575)),
                    ),
                  ),
                );
              },
              itemCount: _bussines.length,
            );
          }
        },
        future: _futureDataBas,
      ),
    );
  }

  Future _refrechData() async {
    return await ApiRequest().getDataBusiness();
  }
}
