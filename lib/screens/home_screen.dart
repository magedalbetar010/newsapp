import 'package:flutter/material.dart';
import 'package:news_app/api/api_get_data.dart';
import 'package:news_app/models/models_api/api_artilces.dart';
import 'package:news_app/screens/business_screen.dart';
import 'package:news_app/screens/science_screen.dart';
import 'package:news_app/screens/sport_screen.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  List<Widget> screen= [BusinessScreen(),SportScreen(),ScienceScreen()];
  int currentIndex =0;
 late Future <List<Articles>> _futureData;
  late Future <List<Articles>> _futureBusinessData;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _futureData = ApiRequest().getDataSport();
     _futureBusinessData=ApiRequest().getDataBusiness();
    _futureBusinessData=ApiRequest().getDataScinec();



  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: screen[currentIndex],
      appBar: AppBar(
        title: const Text("Haberler"),

        backgroundColor: Colors.redAccent,

      ),

      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentIndex,
        onTap: (int value){
          setState(() {
            currentIndex=value;
          });
        },
        backgroundColor: Colors.white,
        selectedItemColor: Colors.redAccent,
        type: BottomNavigationBarType.fixed,

        items:  const [
          BottomNavigationBarItem(
          icon:Icon( Icons.business),
        label: "Işletme",
      ),
          BottomNavigationBarItem(
            icon:Icon( Icons.sports),
            label: "Spor",
          ),
          BottomNavigationBarItem(
            icon:Icon( Icons.science),
            label: "Bilim",
          ),



        ],
      ),
      // floatingActionButton: TextButton(
      //   onPressed: () async{
      //    await getData();
      //   },
      //   child: Text("Get Data"),
      // ),
    );
  }
//   Future getData() async{
//     return await ApiRequest().getDataSport();
// }
}
