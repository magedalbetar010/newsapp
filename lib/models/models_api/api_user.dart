class Items {
  int? id;
  String? fname;
  Null? mname;
  String? family;
  String? username;
  String? password;
  int? active;
  String? regDate;
  Null? country;
  String? email;
  String? authonticationCode;
  Null? confirmCode;

  Items(
      {this.id,
        this.fname,
        this.mname,
        this.family,
        this.username,
        this.password,
        this.active,
        this.regDate,
        this.country,
        this.email,
        this.authonticationCode,
        this.confirmCode});

  Items.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fname = json['fname'];
    mname = json['mname'];
    family = json['family'];
    username = json['username'];
    password = json['password'];
    active = json['active'];
    regDate = json['reg_date'];
    country = json['country'];
    email = json['email'];
    authonticationCode = json['authontication_code'];
    confirmCode = json['confirm_code'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['fname'] = this.fname;
    data['mname'] = this.mname;
    data['family'] = this.family;
    data['username'] = this.username;
    data['password'] = this.password;
    data['active'] = this.active;
    data['reg_date'] = this.regDate;
    data['country'] = this.country;
    data['email'] = this.email;
    data['authontication_code'] = this.authonticationCode;
    data['confirm_code'] = this.confirmCode;
    return data;
  }
}

class First {
  String? ref;

  First({this.ref});

  First.fromJson(Map<String, dynamic> json) {
    ref = json['$ref'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['$ref'] = this.ref;
    return data;
  }
}